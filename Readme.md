Example Usage:

```java
public class TestScript extends Script
{
    BreakHandler breakHandler;
    
    @Override
    public void onStart()
    {
        breakHandler = new BreakHandlerBuilder(this)
                .addEvenDistributedBreakHours(13, 19, 5, 9) // Break every 13-19 hours, for 5-9 hours
                .addEvenDistributedBreakMinutes(55, 90, 5, 20) // Break every 55-90 minutes, for 5-20 minutes
                .build();
    }

    @Override
    public int loop()
    {
        if(breakHandler.shouldBreak())
        {
            // Go to a safe place if your in combat
            breakHandler.performBreak(true);
        }
        else
        {
            // Do what your script does
        }
        return 500;
    }
}
```