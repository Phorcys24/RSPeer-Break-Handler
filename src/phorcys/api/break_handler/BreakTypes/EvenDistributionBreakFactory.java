package phorcys.api.break_handler.BreakTypes;

import org.rspeer.runetek.api.commons.math.Random;
import phorcys.api.break_handler.Break;
import phorcys.api.break_handler.IBreakFactory;


/**
 * Provides a class for generated random breaks, given a minimum and maximum for the values
 */
public class EvenDistributionBreakFactory implements IBreakFactory
{
    private long breakEveryMillisMin, breakEveryMillisMax, breakLengthMin, breakLengthMax;

    public EvenDistributionBreakFactory(long breakEveryMillisMin, long breakEveryMillisMax, long breakLengthMin, long breakLengthMax)
    {
        this.breakEveryMillisMin = breakEveryMillisMin;
        this.breakEveryMillisMax = breakEveryMillisMax;
        this.breakLengthMin = breakLengthMin;
        this.breakLengthMax = breakLengthMax;
    }

    @Override
    public Break generateNextBreak()
    {
        long time = System.currentTimeMillis() + Random.nextLong(breakEveryMillisMin, breakEveryMillisMax);
        long length = Random.nextLong(breakLengthMin, breakLengthMax);

        return new Break(time, length);
    }


    public long getBreakEveryMillisMin()
    {
        return breakEveryMillisMin;
    }

    public void setBreakEveryMillisMin(long breakEveryMillisMin)
    {
        this.breakEveryMillisMin = breakEveryMillisMin;
    }

    public long getBreakEveryMillisMax()
    {
        return breakEveryMillisMax;
    }

    public void setBreakEveryMillisMax(long breakEveryMillisMax)
    {
        this.breakEveryMillisMax = breakEveryMillisMax;
    }

    public long getBreakLengthMin()
    {
        return breakLengthMin;
    }

    public void setBreakLengthMin(long breakLengthMin)
    {
        this.breakLengthMin = breakLengthMin;
    }

    public long getBreakLengthMax()
    {
        return breakLengthMax;
    }

    public void setBreakLengthMax(long breakLengthMax)
    {
        this.breakLengthMax = breakLengthMax;
    }
}
