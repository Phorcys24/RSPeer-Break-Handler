package phorcys.api.break_handler;

import org.rspeer.script.Script;
import phorcys.api.break_handler.BreakTypes.EvenDistributionBreakFactory;
import phorcys.api.break_handler.BreakTypes.NormalDistributionBreakFactory;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Convenient builder class for creating break profiles
 */
public class BreakHandlerBuilder
{
    private Script script;
    private ArrayList<IBreakFactory> breakFactories;

    public BreakHandlerBuilder(Script script)
    {
        this.script = script;
        breakFactories = new ArrayList<>();
    }

    public BreakHandlerBuilder addEvenDistributedBreakMinutes(int breakEveryMin, int breakEveryMax, int breakLengthMin, int breakLengthMax)
    {
        breakFactories.add(new EvenDistributionBreakFactory(
                TimeUnit.MINUTES.toMillis(breakEveryMin),
                TimeUnit.MINUTES.toMillis(breakEveryMax),
                TimeUnit.MINUTES.toMillis(breakLengthMin),
                TimeUnit.MINUTES.toMillis(breakLengthMax)
        ));
        return this;
    }

    public BreakHandlerBuilder addEvenDistributedBreakHours(int breakEveryMin, int breakEveryMax, int breakLengthMin, int breakLengthMax)
    {
        breakFactories.add(new EvenDistributionBreakFactory(
                TimeUnit.HOURS.toMillis(breakEveryMin),
                TimeUnit.HOURS.toMillis(breakEveryMax),
                TimeUnit.HOURS.toMillis(breakLengthMin),
                TimeUnit.HOURS.toMillis(breakLengthMax)
        ));
        return this;
    }

    public BreakHandlerBuilder addNormalDistributedBreakMinutes(int breakEveryMean, int breakEveryStd, int breakLengthMean, int breakLengthStd)
    {
        breakFactories.add(new NormalDistributionBreakFactory(
                TimeUnit.MINUTES.toMillis(breakEveryMean),
                TimeUnit.MINUTES.toMillis(breakEveryStd),
                TimeUnit.MINUTES.toMillis(breakLengthMean),
                TimeUnit.MINUTES.toMillis(breakLengthStd)
        ));
        return this;
    }

    public BreakHandlerBuilder addNormalDistributedBreakHours(int breakEveryMean, int breakEveryStd, int breakLengthMean, int breakLengthStd)
    {
        breakFactories.add(new NormalDistributionBreakFactory(
                TimeUnit.HOURS.toMillis(breakEveryMean),
                TimeUnit.HOURS.toMillis(breakEveryStd),
                TimeUnit.HOURS.toMillis(breakLengthMean),
                TimeUnit.HOURS.toMillis(breakLengthStd)
        ));
        return this;
    }


    public BreakHandler build()
    {
        return new BreakHandler(script, breakFactories.toArray(new IBreakFactory[breakFactories.size()]));
    }
}
