package phorcys.api.break_handler;

/**
 *
 */
public class Break
{
    private long breakTime;
    private long breakLength;

    public Break(long breakTime, long breakLength)
    {
        this.breakTime = breakTime;
        this.breakLength = breakLength;
    }

    public long getBreakTime()
    {
        return breakTime;
    }

    public void setBreakTime(long breakTime)
    {
        this.breakTime = breakTime;
    }

    public long getBreakLength()
    {
        return breakLength;
    }

    public void setBreakLength(long breakLength)
    {
        this.breakLength = breakLength;
    }
}
