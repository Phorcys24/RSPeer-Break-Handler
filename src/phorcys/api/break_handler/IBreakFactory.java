package phorcys.api.break_handler;

/**
 *
 */
public interface IBreakFactory
{
    Break generateNextBreak();
}
