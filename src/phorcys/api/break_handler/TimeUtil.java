package phorcys.api.break_handler;

import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;


public class TimeUtil
{
    public static String convertMilliToHhMmSs(long milliSeconds)
    {
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(milliSeconds),
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) % TimeUnit.MINUTES.toSeconds(1));
    }

    public static long convertHhMmSsToMilli(String time)
    {
        String[] parts = time.split(":");
        if(parts.length == 3)
        {
            return (Integer.parseInt(parts[0])*3600000) +
                    (Integer.parseInt(parts[1])*60000) +
                    (Integer.parseInt(parts[2])*1000);
        }
        return -1;
    }

    public static String perHour(long gained, long runTime)
    {
        return NumberFormat.getIntegerInstance().format(gained * 3600000D / (runTime));
    }
}
