package phorcys.api.break_handler;

import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.script.Script;
import org.rspeer.ui.Log;

import java.util.*;

/**
 *
 */
public class BreakHandler
{
    private HashMap<IBreakFactory, Break> breaks = new HashMap<>();

    private Script script;


    public BreakHandler(Script script)
    {
        this.script = script;
    }

    public BreakHandler(Script script, IBreakFactory... breaks)
    {
        this.script = script;

        for(IBreakFactory b : breaks)
            this.breaks.put(b, b.generateNextBreak());
    }

    /**
     * Determines if it's time to break
     * @return True if the current time is past when any of the breaks are scheduled
     */
    public boolean shouldBreak()
    {
        long currentTime = System.currentTimeMillis();

        return breaks.keySet().stream().anyMatch(key -> currentTime >= breaks.get(key).getBreakTime());
    }

    /**
     * Performs the break by finding the largest active break, waiting until its over, and then generating new break times for any outdated breaks
     * Will exit early if the user stops the script
     * @param shouldLogOut If set to true, this method will attempt to logout before taking the break
     */
    public void performBreak(boolean shouldLogOut)
    {
        long timeout = System.currentTimeMillis() + Random.nextInt(9500, 13500);
        while(shouldLogOut && System.currentTimeMillis() < timeout && Game.isLoggedIn()) // Attempt to log out for the next 10 seconds or so
        {
            if(Game.logout())
                Time.sleepUntil(()-> !Game.isLoggedIn(), Random.nextInt(1250, 2000));
        }

        long currentTime = System.currentTimeMillis();

        // Perform breaking
        breaks.keySet().stream()
                .filter(key ->
                {
                    Break b = breaks.get(key);
                    return currentTime >= b.getBreakTime() && currentTime < b.getBreakTime()+b.getBreakLength();
                })
                .map(key -> breaks.get(key))
                .sorted(Comparator.comparing(Break::getBreakLength).reversed())
                .findFirst() // Gets the longest active break
                .ifPresent(b ->
                {
                    Log.info("Taking break for: " + TimeUtil.convertMilliToHhMmSs(b.getBreakLength()));

                    while(System.currentTimeMillis() <= b.getBreakTime()+b.getBreakLength())
                    {
                        if(script.isStopping()) break;
                        Time.sleep(450, 2100);
                    }
                });

        // Reset any breaks that are outdated
        long afterBreakTime = System.currentTimeMillis();
        breaks.keySet().stream()
                .filter(key ->
                {
                    Break b = breaks.get(key);
                    return afterBreakTime > b.getBreakTime() + b.getBreakLength();
                })
                .forEach(breakFactory ->
                        breaks.put(breakFactory, breakFactory.generateNextBreak()));
    }


    public Set<IBreakFactory> getBreaks()
    {
        return breaks.keySet();
    }

    public void addBreak(IBreakFactory breakFactory)
    {
        breaks.put(breakFactory, breakFactory.generateNextBreak());
    }
}
